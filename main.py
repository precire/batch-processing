import os
from pathlib import Path

import click
import pandas as pd
import requests

RESULTS = [
    "competitive",
    "cooperative",
    "positive",
    "professional",
    "visionary",
]


def precire_call(text, results, doc_type, language, api_key):
    """Get precire results as percentiles for the given text.

    :param text: str with text to be analyzed.
    :param results: list of precire results, that should be returned.
    :param doc_type: str, reference type to use for calculating percentiles.
    :param language: str, either 'de' or 'en'.
    :param api_key: str, precire api key

    :returns: dict with result: percentile as items.
    """
    headers = {
        "Ocp-Apim-Subscription-Key": api_key,
        "Content-Type": "application/json",
        "Content-Language": language,
    }

    result_body = [{"name": res} for res in results]
    body = {"text": {"content": text, "reference": doc_type}, "results": result_body}

    # documentation for the response schema can be found here
    # https://precire.ai/documentation-v2#tag/API-Reference
    response = requests.post("https://api.precire.ai/v2", json=body, headers=headers)

    assert response.status_code == 200, (response.status_code, response.json())
    response = response.json()
    sample = {res: response["results"][res]["percentile"] for res in results}

    return sample


@click.command()
@click.option("-i", "--input_dir", help="directory containing text files")
@click.option("-o", "--outfile", help="file to save output to. Either '.csv' or '.xlsx'")
@click.option("--language", help="document language. 'de' and 'en' are supported.")
def main(input_dir, outfile, language):
    """process text files in SRC_DIR and save the results in OUTFILE."""
    assert "PRECIRE_API_KEY" in os.environ, "Set PRECIRE_API_KEY environment variable"
    api_key = os.environ["PRECIRE_API_KEY"]
    input_dir = Path(input_dir)
    outfile = Path(outfile)
    results = []
    for filename in input_dir.glob("*.txt"):
        text = filename.read_text()
        response = precire_call(text, RESULTS, "default", language, api_key)
        response["file"] = filename.name
        results.append(response)

    df = pd.DataFrame(results)
    df = df[["file"] + RESULTS]
    if outfile.suffix == ".csv":
        df.to_csv(outfile)
    elif outfile.suffix == ".xlsx":
        df.to_excel(outfile)
    else:
        raise ValueError("output type not supported")


if __name__ == "__main__":
    main()
