# batch-processing

This script shows an easy way to call the precire API for many texts.

## Installation

- python 3 and pip are required
- `pip install -r requirements.txt`

## Usage

- set the `PRECIRE_API_KEY` environment variable to your api key.
- call `python main.py --help` to get a help message.
- input is expected to be a directory of text files.

example call:

```bash
python main.py --input_dir data --outfile output.xlsx --language de
```

