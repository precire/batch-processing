Am dritten Tag, als die Eltern und Schwestern fort waren, ging Aschenputtel wieder zu seiner Mutter Grab und sprach zu dem Bäumchen

» Bäumchen, rüttel dich und schüttel dich, wirf Gold und Silber über mich. «

Nun warf ihm der Vogel ein Kleid herab, das war so prächtig und glänzend, wie es noch keins gehabt hatte, und die Pantoffeln waren ganz golden. Als es in dem Kleid zu der Hochzeit kam, wußten sie alle nicht, was sie vor Verwunderung sagen sollten. Der Königssohn tanzte ganz allein mit ihm, und wenn es einer aufforderte, sprach er

» das ist meine Tänzerin. «

